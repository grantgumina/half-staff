﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FlagCheck
{
    class FlagItem
    {
        public string Title { get; private set; }
        public string Link { get; private set; }
        public string Description { get; private set; }
        public DateTime? PubTime { get; private set; }

        private static string GetElementValue(XContainer element, string name)
        {
            if ((element == null) || (element.Element(name) == null))
            {
                return String.Empty;
            }
            return element.Element(name).Value;
        }

        public FlagItem(XContainer item)
        {
            Title = GetElementValue(item, "title");
            Link = GetElementValue(item, "item");
            Description = GetElementValue(item, "description");
            DateTime res;
            if (DateTime.TryParse(GetElementValue(item, "pubDate"), out res))
            {
                PubTime = (DateTime?)res;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}\n\n{1}\n\nPosted at: {2}", Title ?? "no title", Description ?? "Unknown", PubTime.ToString() ?? "unknown");
        }
    }
}
