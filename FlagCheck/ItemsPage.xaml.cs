﻿using FlagCheck.Data;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Data.Xml.Dom;
using System.Xml;
using System.Diagnostics;
using System.Text;
using System.Xml.Linq;
using Windows.UI.ApplicationSettings;

namespace FlagCheck
{
    public sealed partial class ItemsPage : FlagCheck.Common.LayoutAwarePage
    {
        public ItemsPage()
        {
            this.InitializeComponent();
        }

        // PRIVACY POLICY
        // Method to add the privacy policy to the settings charm
        private void ShowPrivacyPolicy(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            SettingsCommand privacyPolicyCommand = new SettingsCommand("privacyPolicy", "Privacy Policy", (uiCommand) => { LaunchPrivacyPolicyUrl(); });
            args.Request.ApplicationCommands.Add(privacyPolicyCommand);
        }

        // Method to launch the url of the privacy policy
        async void LaunchPrivacyPolicyUrl()
        {
            Uri privacyPolicyUrl = new Uri("http://grantgumina.com/halfstaff/privacypolicy.html");
            var result = await Windows.System.Launcher.LaunchUriAsync(privacyPolicyUrl);
        }

        IEnumerable<FlagItem> fItems;
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += ShowPrivacyPolicy;

            XDocument xDoc = XDocument.Load("http://halfstaff.us/rss");
            fItems = from item in xDoc.Descendants("item") select new FlagItem(item);
            
            foreach (var i in fItems)
            {
                statusListView.Items.Add(i.Title);
            }
        }

        private void statusListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ind = statusListView.SelectedIndex;
            string desc = fItems.ToArray()[ind].ToString();
            this.Frame.Navigate(typeof(ItemDetails), desc);
        }
    }
}
