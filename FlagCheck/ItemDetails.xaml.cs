﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace FlagCheck
{
    public sealed partial class ItemDetails : FlagCheck.Common.LayoutAwarePage
    {
        public ItemDetails()
        {
            this.InitializeComponent();
        }

        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            string desc = navigationParameter as string;
            detailsTxt.Text = desc;
        }

        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }
    }
}
